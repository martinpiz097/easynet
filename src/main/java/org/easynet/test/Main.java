package org.easynet.test;

import org.easynet.io.SocketInputStream;
import org.easynet.io.SocketOutputStream;
import org.easynet.net.Client;
import org.easynet.net.Server;
import org.easynet.net.TransferPackage;
import org.easynet.net.interfaces.ServerListener;
import org.easynet.sys.CommandKey;
import org.easynet.sys.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Main {

    private static final int megabyte = 1024*1024;

    public static void main(String[] args) throws Exception {
        //execStreamTest();
        Server server = new Server();
        server.bind(4000);
        server.setServerListener(new ServerListener() {
            @Override
            public void onClientConnected(Socket socket) throws IOException, ClassNotFoundException {
                System.out.println("Cliente conectado: "+socket.getInetAddress()
                        .getHostAddress());
            }

            @Override
            public void onClientDisconnected(long clientId) {

            }
        });
        server.setClientListener(client -> {
            try {
                Log.d(client.recvStr());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });

        server.start();
        Client client = new Client("localhost", 4000);
        client.setListener(cli -> {
            try {
                Log.d(cli.recvStr());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        client.connect();
        client.start();
        client.send("Hello Server!");

        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.print("Msg: ");
            client.send(scan.nextLine());
        }
    }

    private static void execStreamTest() throws IOException, ClassNotFoundException {
        ServerSocket serverSock = new ServerSocket(4000);
        Socket client = new Socket("localhost", 4000);
        Socket accept  = serverSock.accept();

        TransferPackage pkg = new TransferPackage(CommandKey.BYTE, 4);

        SocketOutputStream out = new SocketOutputStream(client.getOutputStream());
        SocketInputStream in = new SocketInputStream(accept.getInputStream());

        System.out.println("Pkg: "+pkg.toSerialString());
        out.writePackage(pkg);

        pkg = in.readPackage();
        System.out.println("PKg Recv: "+pkg.toSerialString());

        System.exit(0);
    }

    public static String createString(int lenght) {
        StringBuilder sbStr = new StringBuilder();
        for (int i = 0; i < lenght/100; i++)
            sbStr.append("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        return sbStr.toString();
    }

    public static void isEquals(String a, String b) {
        char[] ca = a.toCharArray();
        char[] cb = b.toCharArray();
        boolean isEquals;

        if (ca.length != cb.length)
            isEquals = false;

        else for (int i = 0; i < ca.length; i++) {
            if (ca[i]!=cb[i]) {
                isEquals = false;
                break;
            }
        }
        isEquals = true;
        System.out.println(isEquals?"Son Iguales":"Son Distintos");
    }

}
