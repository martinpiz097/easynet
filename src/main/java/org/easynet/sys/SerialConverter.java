package org.easynet.sys;

import java.io.*;

public class SerialConverter {

    private static byte[] extractBytes(String strBytes) {
        String[] splitBytes = strBytes.split(".");
        byte[] bytes = new byte[splitBytes.length];

        for (int i = 0; i < splitBytes.length; i++)
            bytes[i] = Byte.parseByte(splitBytes[i]);

        return bytes;
    }

    private static String convertBytes(byte[] bytes) {
        StringBuilder sbBytes = new StringBuilder();

        for (int i = 0; i < bytes.length; i++)
            sbBytes.append(bytes[i]).append('.');
        sbBytes.deleteCharAt(sbBytes.length()-1);
        return sbBytes.toString();
    }

    public static String toSerialString(SerialData data) throws IOException {
        StringBuilder sbSerial = new StringBuilder();
        sbSerial.append(data.getType());

        if (data.hasData()) {
            sbSerial.append(' ');

            switch (data.getType()) {
                case CommandKey.BYTE:
                    sbSerial.append(data.getParsedData(Number.class).byteValue());
                    break;

                case CommandKey.BYTES:
                    sbSerial.append(convertBytes(data.getParsedData(byte[].class)));
                    break;

                case CommandKey.DOUBLE:
                    sbSerial.append(data.getParsedData(Number.class).doubleValue());
                    break;

                case CommandKey.FLOAT:
                    sbSerial.append(data.getParsedData(Number.class).floatValue());
                    break;

                case CommandKey.INTEGER:
                    sbSerial.append(data.getParsedData(Number.class).intValue());
                    break;

                case CommandKey.LONG:
                    sbSerial.append(data.getParsedData(Number.class).longValue());
                    break;

                case CommandKey.OBJECT:
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(data.getData());
                    sbSerial.append(convertBytes(baos.toByteArray()));
                    baos.close();
                    oos.close();
                    break;

                case CommandKey.SHORT:
                    sbSerial.append(data.getParsedData(Number.class).shortValue());
                    break;

                case CommandKey.STRING:
                    sbSerial.append(data.getData().toString());
                    break;
            }
        }
        return sbSerial.toString();
    }

    public static SerialData deserialize(String serialString) throws IOException, ClassNotFoundException {
        String[] cmdSplit = serialString.split(" ");
        String type = cmdSplit[0];
        SerialData data = new SerialData();
        data.setType(type);

        switch (type) {
            case CommandKey.BYTE:
                data.setParsedData(Byte.parseByte(cmdSplit[1]));
                break;

            case CommandKey.BYTES:
                data.setParsedData(extractBytes(cmdSplit[1]));
                break;

            case CommandKey.DOUBLE:
                data.setParsedData(Double.parseDouble(cmdSplit[1]));
                break;

            case CommandKey.FLOAT:
                data.setParsedData(Float.parseFloat(cmdSplit[1]));
                break;

            case CommandKey.INTEGER:
                data.setParsedData(Integer.parseInt(cmdSplit[1]));
                break;

            case CommandKey.LONG:
                data.setParsedData(Long.parseLong(cmdSplit[1]));
                break;

            case CommandKey.OBJECT:
                ByteArrayInputStream bais = new ByteArrayInputStream(extractBytes(cmdSplit[1]));
                ObjectInputStream ois = new ObjectInputStream(bais);
                Object object = ois.readObject();
                bais.close();
                ois.close();
                data.setData(object);
                break;

            case CommandKey.SHORT:
                data.setParsedData(Short.parseShort(cmdSplit[1]));
                break;

            case CommandKey.STRING:
                int firstSplitLen = cmdSplit[0].length();
                data.setData(serialString.substring(firstSplitLen+1));
                break;
        }
        return data;
    }

}
