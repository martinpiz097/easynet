package org.easynet.sys;

public class SerialData {
    private String type;
    private Object data;

    public SerialData() {}

    public <T> SerialData(String type, T data) {
        this.type = type;
        this.data = data;
    }

    public boolean hasData() {
        return data != null;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public <T> T getParsedData(Class<T> clazz) {
        return (T) data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public <T> void setParsedData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return type + " " + data;
    }
}
