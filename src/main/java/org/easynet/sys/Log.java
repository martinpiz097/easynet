package org.easynet.sys;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Log {

    public static void d(Class<?> clazz, String msg) {
        new Log(clazz, msg).info();
    }

    public static void w(Class<?> clazz, String msg) {
        new Log(clazz, msg).warning();
    }

    public static void e(Class<?> clazz, String msg) {
        new Log(clazz, msg).warning();
    }

    public static void d(Object obj, String msg) {
        d(obj.getClass(), msg);
    }

    public static void w(Object obj, String msg) {
        w(obj.getClass(), msg);
    }

    public static void e(Object obj, String msg) {
        e(obj.getClass(), msg);
    }

    public static void d(String msg) {
        d(null, msg);
    }

    public static void w(String msg) {
        w(null, msg);
    }

    public static void e(String msg) {
        e(null, msg);
    }

    public static enum TYPE {
        INFO, WARNING, ERROR
    }

    private String className;
    private String title;
    private String msg;

    public Log(Class<?> clazz, String title, String msg) {
        this.className = clazz == null ? null : clazz.getName();
        this.title = title;
        this.msg = msg;
    }

    public Log(Class<?> clazz, String msg) {
        this.className = clazz == null ? null : clazz.getName();
        this.msg = msg;
    }

    private String getCurrentTime() {
        StringBuilder sbTime = new StringBuilder();
        Calendar calendar = new GregorianCalendar();
        char timeSeparator = ':';

        byte hour = (byte) calendar.get(Calendar.HOUR_OF_DAY);
        byte minutes = (byte) calendar.get(Calendar.MINUTE);
        byte seconds = (byte) calendar.get(Calendar.SECOND);

        if (hour < 10)
            sbTime.append('0').append(hour);
        else
            sbTime.append(hour);
        sbTime.append(timeSeparator);

        if (minutes < 10)
            sbTime.append('0').append(minutes);
        else
            sbTime.append(minutes);
        sbTime.append(timeSeparator);

        if (seconds < 10)
            sbTime.append('0').append(seconds);
        else
            sbTime.append(seconds);

        return sbTime.toString();
    }

    private StringBuilder getWriter(String color) {
        return new StringBuilder().append(color);
    }

    private String getMsg(StringBuilder sbMsg) {
        sbMsg.append('[').append(getCurrentTime()).append("] ");
        if (className != null)
            sbMsg.append(className).append(": ");
        if (title != null)
            sbMsg.append(title).append(": ");
        sbMsg.append(msg);
        sbMsg.append(ConsoleColor.RESET);
        return sbMsg.toString();
    }

    public void error() {
        System.out.println(getMsg(getWriter(ConsoleColor.ANSI_RED)));
    }

    public void info() {
        System.out.println(getMsg(getWriter(ConsoleColor.ANSI_GREEN)));
    }

    public void warning() {
        System.out.println(getMsg(getWriter(ConsoleColor.ANSI_YELLOW)));
    }

    public void printMsg() {
        info();
    }

    public void printMsg(TYPE type) {
        switch (type) {
            case INFO:
                info();
                break;
            case ERROR:
                error();
                break;
            case WARNING:
                warning();
                break;
        }
    }
}
