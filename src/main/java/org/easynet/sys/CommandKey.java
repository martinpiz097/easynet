package org.easynet.sys;

public class CommandKey {
    public static final String OBJECT = "obj";
    public static final String BYTES = "bytes";
    public static final String BYTE = "byte";
    public static final String SHORT = "sh";
    public static final String INTEGER = "int";
    public static final String LONG = "l";
    public static final String FLOAT = "f";
    public static final String DOUBLE = "d";
    public static final String STRING = "str";
    public static final String CONNECT = "con";
    public static final String DISCONNECT = "dis";
}
