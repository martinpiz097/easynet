package org.easynet.net;

import org.easynet.sys.SerialConverter;
import org.easynet.sys.SerialData;

import java.io.IOException;

public class TransferPackage {
    private SerialData data;


    public TransferPackage(SerialData data) {
        this.data = data;
    }

    public <T> TransferPackage(String type, T data) {
        this.data = new SerialData(type, data);
    }

    // for deserialize
    public TransferPackage(String serialString)
            throws IOException, ClassNotFoundException {
        this.data = toObject(serialString);
    }

    private SerialData toObject(String serialString)
            throws IOException, ClassNotFoundException {
        return SerialConverter.deserialize(serialString);
    }

    public String toSerialString() throws IOException {
        return SerialConverter.toSerialString(data);
    }

    public SerialData getData() {
        return data;
    }
}
