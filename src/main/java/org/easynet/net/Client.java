package org.easynet.net;

import org.easynet.exceptions.ListenerNotDefinedException;
import org.easynet.io.SocketInputStream;
import org.easynet.io.SocketOutputStream;
import org.easynet.net.interfaces.Connectable;
import org.easynet.net.interfaces.ClientListener;
import org.easynet.sys.CommandKey;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import static org.easynet.sys.CommandKey.CONNECT;
import static org.easynet.sys.CommandKey.DISCONNECT;

public class Client extends Thread implements Connectable {
    private final Socket socket;
    private final SocketOutputStream outputStream;
    private final SocketInputStream inputStream;

    private ClientListener clientListener;

    private boolean connected;

    private long serverId;

    public Client(String host, int port) throws IOException {
        this(new Socket(host, port));
    }

    public Client(InetAddress address, int port) throws IOException {
        this(new Socket(address, port));
    }

    public Client(Socket socket) throws IOException {
        this(socket,
                new SocketOutputStream(socket.getOutputStream()),
                new SocketInputStream(socket.getInputStream()));
    }

    public Client(Socket socket,
                  SocketOutputStream outputStream, SocketInputStream inputStream) {
        this.socket = socket;
        this.outputStream = outputStream;
        this.inputStream = inputStream;
        serverId = Integer.MIN_VALUE;
    }

    Client(Socket socket, long serverId) throws IOException {
        this(socket,
                new SocketOutputStream(socket.getOutputStream()),
                new SocketInputStream(socket.getInputStream()), serverId);
    }

    Client(Socket socket,
                  SocketOutputStream outputStream,
           SocketInputStream inputStream, long serverId) {
        this.socket = socket;
        this.outputStream = outputStream;
        this.inputStream = inputStream;
        this.serverId = serverId;
    }

    private void sendPackage(TransferPackage pkg) throws IOException {
        outputStream.writePackage(pkg);
    }

    private TransferPackage recvPkg() throws IOException, ClassNotFoundException {
        return inputStream.readPackage();
    }

    private void closeAll() throws IOException {
        outputStream.close();
        inputStream.close();
        socket.close();
    }

    public void setListener(ClientListener listener) {
        this.clientListener = listener;
    }

    public Socket getSocket() {
        return socket;
    }

    public SocketOutputStream getOutputStream() {
        return outputStream;
    }

    public SocketInputStream getInputStream() {
        return inputStream;
    }

    public ClientListener getClientListener() {
        return clientListener;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public boolean isConnected() {
        return connected;
    }

    void unlink() throws IOException {
        closeAll();
    }

    @Override
    public void connect() throws IOException {
        sendPackage(new TransferPackage(CONNECT, null));
        connected = true;
    }

    @Override
    public void disconnect() throws IOException {
        sendPackage(new TransferPackage(DISCONNECT, null));
        connected = false;
    }

    @Override
    public void send(byte[] data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.BYTES, data));
    }

    @Override
    public void send(byte[] data, int offset, int lenght) throws IOException {
        byte[] newBuff = new byte[lenght];
        System.arraycopy(data, offset, newBuff, 0, lenght);
        sendPackage(new TransferPackage(CommandKey.BYTES, newBuff));
    }

    @Override
    public void send(String data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.STRING, data));
    }

    @Override
    public void send(Object data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.OBJECT, data));
    }

    @Override
    public void send(char data) throws IOException {
        send((byte)data);
    }

    @Override
    public void send(byte data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.BYTE, data));
    }

    @Override
    public void send(short data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.SHORT, data));
    }

    @Override
    public void send(int data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.INTEGER, data));
    }

    @Override
    public void send(long data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.LONG, data));
    }

    @Override
    public void send(float data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.FLOAT, data));
    }

    @Override
    public void send(double data) throws IOException {
        sendPackage(new TransferPackage(CommandKey.DOUBLE, data));
    }

    @Override
    public byte[] recvBuffer() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(byte[].class);
    }

    @Override
    public String recvStr() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(String.class);
    }

    @Override
    public Object recvObject() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getData();
    }

    @Override
    public char recvChar() throws IOException, ClassNotFoundException {
        return (char)recvByte();
    }

    @Override
    public byte recvByte() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(byte.class);
    }

    @Override
    public short recvShort() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(short.class);
    }

    @Override
    public int recvInt() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(int.class);
    }

    @Override
    public long recvLong() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(long.class);
    }

    @Override
    public float recvFloat() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(float.class);
    }

    @Override
    public double recvDouble() throws IOException, ClassNotFoundException {
        return recvPkg().getData().getParsedData(double.class);
    }

    @Override
    public void run() {
        if (clientListener == null)
            throw new ListenerNotDefinedException();
        connected = true;
        while (connected) {
            try {
                if (inputStream.available() > 0)
                    clientListener.onRequest(this);
                Thread.sleep(1);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
