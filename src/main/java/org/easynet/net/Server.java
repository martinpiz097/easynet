package org.easynet.net;

import org.easynet.exceptions.ListenerNotDefinedException;
import org.easynet.io.SocketInputStream;
import org.easynet.net.interfaces.ClientListener;
import org.easynet.net.interfaces.ServerListener;
import org.easynet.sys.CommandKey;
import org.easynet.sys.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Server extends Thread {
    private ServerSocket serverSocket;
    private LinkedList<Client> listClients;

    private ServerListener serverListener;
    private ServerListener defaultListener;
    private ClientListener clientListener;

    private boolean connected;

    static LinkedList<Server> listInstancedServers;

    static {
        listInstancedServers = new LinkedList<>();
    }

    public Server() throws IOException {
        serverSocket = new ServerSocket();
        listClients = new LinkedList<>();
        listInstancedServers.add(this);
        configDefaultListener();
    }

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        listClients = new LinkedList<>();
        listInstancedServers.add(this);
        configDefaultListener();
    }

    private void configDefaultListener() {
        defaultListener = new ServerListener() {
            @Override
            public void onClientConnected(Socket socket) throws IOException, ClassNotFoundException {
                Log.d(this, "Client connected from "+socket.getInetAddress().getHostAddress());
                SocketInputStream inputStream = new SocketInputStream(socket.getInputStream());
                TransferPackage pkg = inputStream.readPackage();
                System.out.println("Petition Received: "+pkg.getData().toString());
                if (pkg.getData().getType().equals(CommandKey.CONNECT)) {
                    serverListener.onClientConnected(socket);
                    Log.d(this, "Client "+addClient(new Client(socket, getId()))+
                            " authorized");
                }
                else
                    Log.e(this, "Host "+socket.getInetAddress().getHostAddress()+" not authorized");
            }

            @Override
            public void onClientDisconnected(long clientId) {
                serverListener.onClientDisconnected(clientId);
                removeClient(clientId);
                Log.d(this, "Client "+clientId+" disconnected");
            }
        };
    }

    private void closeAll() {
        listClients.parallelStream().forEach(cli->{
            try {
                cli.unlink();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        listInstancedServers.removeIf(srv->srv.getId()==getId());
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(this, "Server closed");
        Log.d(this, "Port "+serverSocket.getLocalPort()+" released");

    }

    public void bind(int port) throws IOException {
        serverSocket.bind(new InetSocketAddress(port));
    }

    public synchronized void disconnect() {
        connected = false;
    }

    private synchronized long addClient(Client client) {
        client.setListener(clientListener);
        if (!client.isAlive())
            client.start();
        listClients.add(client);
        return client.getId();
    }

    private synchronized void removeClient(long clientId) {
        listClients.parallelStream().filter(
                (cli)->cli.getId() == clientId)
                .iterator().remove();
        Log.d(this, "Client "+clientId+" removed");
    }

    // se entrega copia para no provocar errores
    public synchronized LinkedList<Client> getListClients() {
        return new LinkedList<>(listClients);
    }

    public synchronized ServerListener getServerListener() {
        return serverListener;
    }

    public synchronized void setServerListener(ServerListener serverListener) {
        this.serverListener = serverListener;
    }

    public synchronized ClientListener getClientListener() {
        return clientListener;
    }

    public synchronized void setClientListener(ClientListener clientListener) {
        this.clientListener = clientListener;
    }

    @Override
    public void run() {
        if (serverListener == null || clientListener == null)
            throw new ListenerNotDefinedException();

        connected = true;
        Log.d(this, "Server started in port "+serverSocket.getLocalPort());
        while (connected) {
            try {
                Log.d(this, "Listening clients...");
                defaultListener.onClientConnected(serverSocket.accept());
                Thread.sleep(1);
            } catch (IOException | ClassNotFoundException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        closeAll();
    }

}
