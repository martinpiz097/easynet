package org.easynet.net.interfaces;

import org.easynet.net.Client;

@FunctionalInterface
public interface ClientListener {
    public void onRequest(Client client);
}
