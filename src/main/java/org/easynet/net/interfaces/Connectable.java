package org.easynet.net.interfaces;

import java.io.IOException;

public interface Connectable {

    public void connect() throws IOException;
    public void disconnect() throws IOException;

    public void send(byte[] data) throws IOException;
    public void send(byte[] data, int offset, int lenght) throws IOException;
    public void send(String data) throws IOException;
    public void send(Object data) throws IOException;
    public void send(char data) throws IOException;
    public void send(byte data) throws IOException;
    public void send(short data) throws IOException;
    public void send(int data) throws IOException;
    public void send(long data) throws IOException;
    public void send(float data) throws IOException;
    public void send(double data) throws IOException;


    public byte[] recvBuffer() throws IOException, ClassNotFoundException;
    public String recvStr() throws IOException, ClassNotFoundException;
    public Object recvObject() throws IOException, ClassNotFoundException;
    public char recvChar() throws IOException, ClassNotFoundException;
    public byte recvByte() throws IOException, ClassNotFoundException;
    public short recvShort() throws IOException, ClassNotFoundException;
    public int recvInt() throws IOException, ClassNotFoundException;
    public long recvLong() throws IOException, ClassNotFoundException;
    public float recvFloat() throws IOException, ClassNotFoundException;
    public double recvDouble() throws IOException, ClassNotFoundException;
}
