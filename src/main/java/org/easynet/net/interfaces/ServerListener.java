package org.easynet.net.interfaces;

import java.io.IOException;
import java.net.Socket;

public interface ServerListener {
    public void onClientConnected(Socket socket) throws IOException, ClassNotFoundException;
    public void onClientDisconnected(long clientId);
}
