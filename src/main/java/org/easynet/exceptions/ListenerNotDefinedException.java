package org.easynet.exceptions;

public class ListenerNotDefinedException extends RuntimeException {
    public ListenerNotDefinedException() {
        super("Almost one listener is not defined");
    }
}
