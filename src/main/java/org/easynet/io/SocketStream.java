package org.easynet.io;

import org.easynet.io.interfaces.StreamCommunicable;
import org.easynet.net.TransferPackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SocketStream implements StreamCommunicable {
    private final SocketOutputStream outputStream;
    private final SocketInputStream inputStream;

    public SocketStream(SocketOutputStream outputStream, SocketInputStream inputStream) {
        this.outputStream = outputStream;
        this.inputStream = inputStream;
    }

    public SocketStream(OutputStream outputStream, InputStream inputStream) throws IOException {
        this(new SocketOutputStream(outputStream), new SocketInputStream(inputStream));
    }

    public SocketOutputStream getOutputStream() {
        return outputStream;
    }

    public SocketInputStream getInputStream() {
        return inputStream;
    }

    @Override
    public String readString() throws IOException {
        return inputStream.readString();
    }

    @Override
    public byte[] readBytes() throws IOException {
        return inputStream.readBytes();
    }

    @Override
    public TransferPackage readPackage() throws IOException, ClassNotFoundException {
        return inputStream.readPackage();
    }

    @Override
    public void writeString(String str) throws IOException {
        outputStream.writeString(str);
    }

    @Override
    public void writeBytes(byte[] data) throws IOException {
        outputStream.writeBytes(data);
    }

    @Override
    public void writePackage(TransferPackage pkg) throws IOException {
        outputStream.writePackage(pkg);
    }

}
