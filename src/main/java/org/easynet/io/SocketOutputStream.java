package org.easynet.io;


import org.easynet.io.interfaces.TransmissibleStream;
import org.easynet.net.TransferPackage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author martin
 */

// El problema en android podria deberse al buffersize del socket
public class SocketOutputStream extends DataOutputStream implements TransmissibleStream {

    public SocketOutputStream(OutputStream socketStream) throws IOException {
        super(socketStream);
    }

    protected void writeData(byte[] data) throws IOException {
        //data = encryptor.encrypt(data);
        int dataLen = data.length;
        /*if (dataLen > BUFFSIZE) {
            ByteBuffer buffer = new ByteBuffer(data);
            int packets = buffer.size() / BUFFSIZE;
            int residue = buffer.size() % BUFFSIZE;
            //writeShort(residue > 0 ? HASRES : NORES);

            for (int i = 0; i < packets; i++)
                write(buffer.read(i*BUFFSIZE, BUFFSIZE));

            if (residue > 0)
                write(buffer.read(packets*BUFFSIZE));
        }
        else*/
        writeInt(dataLen);
        write(data);
        flush();
    }

    @Override
    public void writeString(final String str) throws IOException {
        writeData(str.getBytes("UNICODE"));
    }

    @Override
    public void writeBytes(final byte[] data) throws IOException {
        writeData(data);
    }

    @Override
    public void writePackage(TransferPackage pkg) throws IOException {
        writeString(pkg.toSerialString());
    }

    /*
    public static void main(String[] args) {
        int test = 64;
        // >>> ó >> --> dividir por dos las veces que el numero lo defina
        // << --> multiplicar por dos
        System.out.println(test<<1);
        System.out.println(0xFF);
        System.out.println(-1 & 0xFF);
        int i = 1000;
        System.out.println("--------------------");
        System.out.println(i>>>24);
        System.out.println(i>>>16);
        System.out.println(i>>>8);
        System.out.println(i>>>0);
        System.out.println("--------------------");
        int i1 = i>>>24 & 255;
        int i2 = i>>>16 & 255;
        int i3 = i>>>8 & 255;
        int i4 = i>>>0 & 255;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
        System.out.println("--------------------");
        i1 = i1 << 24;
        i2 = i2 << 16;
        i3 = i3 << 8;
        i4 = i4 << 0;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
        System.out.println(i1+i2+i3+i4);
    }

    */


}
