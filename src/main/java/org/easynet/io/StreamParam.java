package org.easynet.io;

public class StreamParam {
    public static final int BUFFSIZE = (int) Math.pow(1024, 2);

    public static final short HASPKG = 1020;
    public static final short NOPKG = 1021;
    public static final short HASRES = 1022;
    public static final short NORES = 1023;

}
