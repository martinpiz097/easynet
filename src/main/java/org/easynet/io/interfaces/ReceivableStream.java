package org.easynet.io.interfaces;

import org.easynet.net.TransferPackage;

import java.io.IOException;

public interface ReceivableStream {
    public String readString() throws IOException;
    public byte[] readBytes() throws IOException;
    public TransferPackage readPackage() throws IOException, ClassNotFoundException;
}
