package org.easynet.io.interfaces;

public interface StreamCommunicable
        extends TransmissibleStream, ReceivableStream {}
