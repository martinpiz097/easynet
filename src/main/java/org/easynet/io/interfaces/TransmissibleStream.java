package org.easynet.io.interfaces;

import org.easynet.net.TransferPackage;

import java.io.IOException;

public interface TransmissibleStream {
    public void writeString(final String str) throws IOException;
    public void writeBytes(final byte[] data) throws IOException;
    public void writePackage(TransferPackage pkg) throws IOException;
}
