package org.easynet.io;

import org.easynet.io.interfaces.ReceivableStream;
import org.easynet.net.TransferPackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;

import static org.easynet.io.StreamParam.HASPKG;
import static org.easynet.io.StreamParam.HASRES;

/**
 *
 * @author martin
 */

public class SocketInputStream extends DataInputStream implements ReceivableStream {

    //private boolean enableWait;

    public SocketInputStream(InputStream socketStream) {
        super(socketStream);
        //enableWait = true;
    }

    protected void waitForData() throws IOException {
        while (available() == 0) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean hasPackages() throws IOException {
        return readShort() == HASPKG;
    }

    protected boolean hasResidue() throws IOException {
        return readShort() == HASRES;
    }

    protected byte[] readData() throws IOException {
        //if (enableWait)
            waitForData();
        try {
            int available = readInt();
            byte[] buffer = new byte[available];
            for (int i = 0; i < available; i++)
                buffer[i] = (byte) read();
            //System.out.println("Recd Data: "+new String(buffer));

            return buffer;
        } catch (SocketTimeoutException e) {
            return null;
        }
    }

    @Override
    public String readString() throws IOException {
        String str = new String(readData(), "UNICODE");
        return str;
    }

    @Override
    public byte[] readBytes() throws IOException {
        return readData();
    }

    @Override
    public TransferPackage readPackage() throws IOException, ClassNotFoundException {
        return new TransferPackage(readString());
    }

}
